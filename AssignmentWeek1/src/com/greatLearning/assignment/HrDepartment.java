package com.greatLearning.assignment;

public class HrDepartment extends SuperDepartment {
	public String deparmentName() {
		return " HR Department ";
		}
		//declare method getTodaysWork of return type string
		public String getTodaysWork() {
		return " Fill todays worksheet and mark your attendance";
		}
		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
		return " Complete by EOD ";
		
		}
		//declare method getWorkDeadline of return type string
				public String doActivity() {
				return " Team Lunch. ";
				
				}
				@Override
				public String toString() {
					return "DeparmentName = " + deparmentName() + ", getTodaysWork = " + getTodaysWork()
							+ ", getWorkDeadline = " + getWorkDeadline() + ", doActivity = " + doActivity()		+ ", isTodayAHoliday = " +"\n "+ isTodayAHoliday();
				}
				
}
